var request = require("supertest");
var should = require("should");
var expect = require("chai").expect;
var app = require('../../server/index.js');

describe('GET /api/movies', function(){
    it('should return code 200', function(done) {
        request(app)
            .get('/api/movies')
            .expect(200)
            .end(function(err, res){
                if(err) {
                    done(err);
                } else {
                    done();
                }
            });
    });

    it('should contain content element within response', function(done){
        request(app)
            .get('/api/movies')
            .end(function(err, res){
                should.exist(res.body.content);
                done();
            });
    });

    it('should return first page of results', function(done){
        request(app)
            .get('/api/movies')
            .end(function(err, res){
                var data = JSON.parse(res.text);
                expect(data.pageNumber).to.equal(1);
                done();
            });
    });

    it('content element should only contain 20 items', function(done){
        request(app)
            .get('/api/movies')
            .end(function(err, res){
                var data = JSON.parse(res.text);
                expect(data.content.length).to.equal(20);
                done();
            });
    });

    it('response.totalItems should equal 160', function(done){
        request(app)
            .get('/api/movies')
            .end(function(err, res){
                var data = JSON.parse(res.text);
                expect(data.totalItems).to.equal(160);
                done();
            });
    });

    it('response.matchedItems should equal 160', function(done){
        request(app)
            .get('/api/movies')
            .end(function(err, res){
                var data = JSON.parse(res.text);
                expect(data.matchedItems).to.equal(160);
                done();
            });
    });

    it('response.totalPages should equal 8', function(done){
        request(app)
            .get('/api/movies')
            .end(function(err, res){
                var data = JSON.parse(res.text);
                expect(data.totalPages).to.equal(8);
                done();
            });
    });

    it('response.startItem should equal 1', function(done){
        request(app)
            .get('/api/movies')
            .end(function(err, res){
                var data = JSON.parse(res.text);
                expect(data.startItem).to.equal(1);
                done();
            });
    });

    it('response.endItem should equal 20', function(done){
        request(app)
            .get('/api/movies')
            .end(function(err, res){
                var data = JSON.parse(res.text);
                expect(data.endItem).to.equal(20);
                done();
            });
    });

});